using System;
using System.Collections;
using Android.App;
using Android.Content;
using Android.Widget;
using System.Text;

namespace CombatTriangle
{
    public class ChampBtn 
    {
        public bool IsValid = false;
        public string selection = "";

	    static string[] fighters = {"Melee", "Ranged", "Magic"}; 
        int currIndex;
        Button b; 

        public ChampBtn(Button btn)
        {
            b = btn;
            b.Click += ChangeChampion;
        }

        protected void ChangeChampion(object sender, EventArgs e)
        {
            currIndex = Array.IndexOf(fighters, b.Text);
            if(currIndex < 0 || currIndex == fighters.Length - 1)
            {
                currIndex = 0;
            }else if(currIndex < fighters.Length - 1)
            {
                currIndex++;
            }
            IsValid = true;
            selection = fighters[currIndex];
            b.Text = selection;
        }

        public static string GenerateOpponent()
        {
            string selected;
            Random r = new Random();
            StringBuilder sb = new StringBuilder();
            for(int i=0; i < 3; i++)
            {
                sb.Append(fighters[r.Next(0, 3)]);
                if(i != 2)
                {
                    sb.Append(',');
                } 
            }
            selected = sb.ToString(); 
            return selected;
        }

    }
}
