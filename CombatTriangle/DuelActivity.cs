using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace CombatTriangle
{
    [Activity(Label = "Duel Results")]
    public class DuelActivity : Activity
    {
        Dictionary<string, Tuple<string, string>> gamerules;
        List<TextView> output = new List<TextView>();
        string[] playerSelected;
        string[] opponentSelected;
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            playerSelected = Intent.GetStringExtra("playerselected").Split(',');
            opponentSelected = Intent.GetStringExtra("opponentselected").Split(',');
            SetContentView(Resource.Layout.Duel);

            gamerules = new Dictionary<string, Tuple<string, string>>();
            // Key: Player Selection                Value: Wins against, Loses Against
            gamerules.Add("Melee", new Tuple<string, string>("Ranged", "Magic"));
            gamerules.Add("Ranged", new Tuple<string, string>("Magic", "Melee"));
            gamerules.Add("Magic", new Tuple<string, string>("Melee", "Ranged"));

            output.Add(FindViewById<TextView>(Resource.Id.duel1));
            output.Add(FindViewById<TextView>(Resource.Id.duel2));
            output.Add(FindViewById<TextView>(Resource.Id.duel3));

            Duel();
        }

        protected void Duel()
        {
            for(int i = 0; i < output.Count; i++)
            {
                if(gamerules[playerSelected[i]].Item1 == opponentSelected[i])
                {
                    output[i].Text = "You win this one!";
                    output[i].SetBackgroundColor(Color.DarkGreen);
                }
                else if (gamerules[playerSelected[i]].Item2 == opponentSelected[i])
                {
                    output[i].Text = "You lose!";
                    output[i].SetBackgroundColor(Color.DarkRed);
                }
                else
                {
                    output[i].Text = "Too Boring to watch";
                }
            }

        }

    }
}