﻿using System;
using System.Collections;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Text;

namespace CombatTriangle
{
    [Activity(Label = "CombatTriangle", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.MyButton);
            ChampBtn champ1 = new ChampBtn(FindViewById<Button>(Resource.Id.champ1));
            ChampBtn champ2 = new ChampBtn(FindViewById<Button>(Resource.Id.champ2));
            ChampBtn champ3 = new ChampBtn(FindViewById<Button>(Resource.Id.champ3));
            button.Click += delegate {
                if (champ1.IsValid && champ2.IsValid && champ3.IsValid)
                {
                    var duel = new Intent(this,typeof(DuelActivity));
                    StringBuilder sb = new StringBuilder();
                    sb.Append(champ1.selection);
                    sb.Append("," + champ2.selection);
                    sb.Append("," + champ3.selection);
                    string selected = sb.ToString();
                    string opponent = ChampBtn.GenerateOpponent();
                    duel.PutExtra("playerselected", selected);
                    duel.PutExtra("opponentselected", opponent);
                    StartActivity(duel);
                }
  

            }; 
        } 

    }
}

